#!/usr/bin/python3

import requests
import click
import random

HTTP_ENDPOINT = "https://mtr.sh"


def probe_group(probe, group):
    return probe[1]["group"] == group


def probe_cap(probe, cap):
    return probe[1]["caps"].get(cap)


def get_probes(probe_filter=lambda x: True):
    r = requests.get("{url}/probes.json".format(url=HTTP_ENDPOINT))
    probes = r.json()
    return dict(filter(probe_filter, probes.items()))


@click.command()
@click.argument("target", required=True)
@click.option(
    "-t",
    "--test",
    type=click.Choice(["mtr", "bgp", "ping"]),
    default="mtr",
    help="Looking glass test.",
)
@click.option(
    "-g", "--group", default="Europe", help="Probes' group, i.e the continent."
)
@click.option("-c", "--count", default=2, help="Number of probes to query.")
def main(target, test, group, count):
    def probe_filter(probe):
        # fmt: off
        return all(
            [
                probe_group(probe, group),
                probe_cap(probe, test)
            ]
        )
        # fmt: on

    probes = get_probes(probe_filter)
    probes_list = list(probes.items())
    random.shuffle(probes_list)

    for probe in probes_list[:count]:
        probe_id = probe[0]
        print(
            'Probe "{}" in {}, {}, AS{}:'.format(
                probe_id,
                probe[1]["city"],
                probe[1]["country"],
                probe[1]["asnumber"],
            )
        )
        r = requests.get(
            "{url}/{probe_id}/{cap}/{target_ip}".format(
                url=HTTP_ENDPOINT,
                probe_id=probe_id,
                cap=test,
                target_ip=target,
            )
        )
        print(r.text)


if __name__ == "__main__":
    main()
