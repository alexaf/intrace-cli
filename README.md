# intrace-cli

A simple python3 cli tool to interact with mtr.sh, a great service
powered by https://github.com/Fusl/intrace.

## Usage

Just provide a target IPv4 or IPv6 address:

`./intrace-cli.py <target IP address>`

By default it will return the results of `mtr` test from *2* looking glass
probes in `Europe`. This can be tweaked by the respective options:

```
  -t, --test [mtr|bgp|ping]  Looking glass test.
  -g, --group TEXT           Probes' group, i.e the continent.
  -c, --count INTEGER        Number of probes to query.
```
